$oldKeyVault = 'Old-Vault'
$newKeyVault = 'New-Vault'

#Display Secrets in New Key Vault
Write-Host 'Secrets in New Key Vault BEFORE Sync'
$newSecrets = Get-AzKeyVaultSecret -VaultName $newKeyVault
foreach($newSecret in $newSecrets)
{
    $newSecretDetails = Get-AzKeyVaultSecret -VaultName $newKeyVault -Name $newSecret.Name

    Write-Host 'New Secret: Name='$newSecretDetails.Name

    #Uncomment below if you need to check values
    #Write-Host 'New Secret: Name='$newSecretDetails.Name ': Value=' $newSecretDetails.SecretValueText
}

Write-Host ''
Write-Host ''

#Display Secrets in New Old Vault
Write-Host 'Secrets in Old Key Vault before Sync'
$oldSecrets = Get-AzKeyVaultSecret -VaultName $oldKeyVault
foreach($oldSecret in $oldSecrets)
{
    $oldSecretDetails = Get-AzKeyVaultSecret -VaultName $oldKeyVault -Name $oldSecret.Name
    
    Write-Host 'Old Secret: Name='$oldSecretDetails.Name

    #Uncomment below if you need to check values   
    #Write-Host 'Old Secret: Name='$oldSecretDetails.Name ': Value=' $oldSecretDetails.SecretValueText
}

Write-Host ''
Write-Host ''

#Sync Key Vault
Write-Host 'Syncing Vaults'
$oldSecrets = Get-AzKeyVaultSecret -VaultName $oldKeyVault
foreach($oldSecret in $oldSecrets)
{
    $oldSecretDetails = Get-AzKeyVaultSecret -VaultName $oldKeyVault -Name $oldSecret.Name    
    $secureStringKey = ConvertTo-SecureString -String $oldSecretDetails.SecretValueText -AsPlainText -Force    
    Set-AzKeyVaultSecret -VaultName $newKeyVault -Name $oldSecretDetails.Name -SecretValue $secureStringKey
    Write-Host 'Secret Copied to New Key Vault: Name = ' $oldSecretDetails.Name
}

Write-Host ''
Write-Host ''

#Display Secrets in New Key Vault
Write-Host 'Secrets in New Key Vault AFTER Sync'
$newSecrets = Get-AzKeyVaultSecret -VaultName $newKeyVault
foreach($newSecret in $newSecrets)
{
    $newSecretDetails = Get-AzKeyVaultSecret -VaultName $newKeyVault -Name $newSecret.Name

    Write-Host 'New Secret: Name='$newSecretDetails.Name

    #Uncomment below if you need to check values  
    #Write-Host 'New Secret: Name='$newSecretDetails.Name ': Value=' $newSecretDetails.SecretValueText
}