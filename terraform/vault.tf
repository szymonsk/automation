##                     ##
#    Create a vault     #
##                     ##
data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "kv" {
  name                       = var.key_vault_name
  location                   = azurerm_resource_group.inspire.location
  resource_group_name        = azurerm_resource_group.inspire.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  sku {
    tier = "Premium"
    # size = "S1"
  }
  soft_delete_enabled        = true
  soft_delete_retention_days = 90

  access_policy {
    tenant_id                = data.azurerm_client_config.current.tenant_id
    object_id                = data.azurerm_client_config.current.object_id

    key_permissions = [
      "list",
      "create",
      "get"
    ]

    secret_permissions = [
      "list",
      "set"
    ]
  }
}

resource "azurerm_key_vault_secret" "kv_secret" {
  name         = "test-secret"
  value        = "123321"
  key_vault_id = azurerm_key_vault.kv.id
}


##                            ##
# Create a vault's certificate #
##                            ##
resource "azurerm_key_vault_certificate" "example" {
  name         = "imported-cert"
  key_vault_id = azurerm_key_vault.example.id

  certificate {
    contents = filebase64("certificate-to-import.pfx")
    password = ""
  }

  certificate_policy {
    issuer_parameters {
      name = "Self"
    }

    key_properties {
      exportable = true
      key_size   = 4096
      key_type   = "RSA"
      reuse_key  = false
    }

    secret_properties {
      content_type = "application/x-pkcs12"
    }
  }
}