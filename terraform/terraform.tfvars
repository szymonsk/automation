resource_group_name = "inspiretestresourcegroup3a6cb81d"
resource_group_location = "northcentralus"
storage_account_name = "inspireteststacc3a6cb81d"
blob_container_name = "inspiretestserviceplan3a6cb81d"
key_vault_name = "inspiretestkeyvault3a6cb81d"
app_service_name = "inspiretestapp3a6cb81d"
session_duration = 999
device_type_connection_string = "inspiredevice"
custom_domain_app_service_url = "https://localhost/"
ip_restriction_address = "8.8.8.8"

sql_database_server_name = "inspiretestdbserver3a6cb81d"
sql_database_name = "inspiretestdatabase3a6cb81d"
