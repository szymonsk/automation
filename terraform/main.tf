terraform {

  required_version = ">=0.12"

  required_providers {
    azurerm ={
        version="2.80.0"
         source="hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
#   skip_provider_registration = true
  features {}
}

resource "azurerm_resource_group" "inspire" {
  name     = var.resource_group_name
  location = var.resource_group_location
  tags     = {
      "terrafrom" = "terraform"
  }
}

resource "azurerm_storage_account" "inspire" {
  name                     = var.storage_account_name
  resource_group_name      = azurerm_resource_group.inspire.name
  location                 = azurerm_resource_group.inspire.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_storage_container" "inspire" {
  name                  = var.blob_container_name
  storage_account_name  = azurerm_storage_account.inspire.name
  container_access_type = "private"
}

##                     ##
# Create a service plan #
##                     ##
resource "azurerm_app_service_plan" "inspire" {
  name                = var.app_service_plan_name
  location            = azurerm_resource_group.inspire.location
  resource_group_name = azurerm_resource_group.inspire.name

  sku {
    tier = "Standard"
    size = "S1"
  }
}

##                     ##
#  Create app service   #
##                     ##
resource "azurerm_app_service" "inspire" {
  name                = var.app_service_name
  location            = azurerm_resource_group.inspire.location
  resource_group_name = azurerm_resource_group.inspire.name
  app_service_plan_id = azurerm_app_service_plan.inspire.
  
  https_only              = true
  client_affinity_enabled = true
  client_cert_enabled     = true

  app_settings = {
    "KeyVaultBaseUrl" = "https://${var.key_vault_name}.vault.azure.net/"
    "SessionDuration" = var.session_duration
  }

  connection_string {
    name  = var.device_type_connection_string
    type  = "SQLServer"
    value = <<EOT
      "Server=tcp:${var.sql_server_name}.database.windows.net,1433;
      Initial Catalog=${var.sql_database_name};
      MultipleActiveResultSets=False;
      Encrypt=True;
      TrustServerCertificate=False;
      Connection Timeout=30; 
      UID=AnyString;
      Authentication=Active Directory Interactive;"
    EOT
  }
      
  site_config {
    dotnet_framework_version  = "v4.0"
    use_32_bit_worker_process = true
    managed_pipeline_mode     = "Integrated"
    ftps_state                = "Disabled"
    http2_enabled             = false
    websockets_enabled        = false
    always_on                 = true
    min_tls_version = 1.2
    ip_restriction = [
      {
        name                      = "IP Restriction"
        action                    = "Allow"
        ip_address                = "10.0.0.0/32"
        virtual_network_subnet_id = null
        subnet_id                 = null
        service_tag               = null
        headers                   = []
        priority                  = 1000
      }
    ]
  }

  backup {
    name                = "app-backup"
    enabled             = false
    storage_account_url = "https://myaccount.blob.core.windows.net/sascontainer/sasblob.txt?sv=2019-02-02&st=2019-04-29T22%3A18%3A26Z&se=2019-04-30T02%3A23%3A26Z&sr=b&sp=rw&sip=168.1.5.60-168.1.5.70&spr=https&sig=Z%2FRHIX5Xcg0Mq2rqI3OlWTjEg2tYkboXr1P9ZUXDtkk%3D"
    schedule {
      frequency_interval       = 1
      frequency_unit           = "Day"
      start_time               = null  # RFC 3339, make a fuction to get current
      retention_period_in_days = 30
      keep_at_least_one_backup = true
    }
  }
}

resource "azurerm_app_service_custom_hostname_binding" "inspire" {
  hostname            = var.custom_domain_app_service_url
  app_service_name    = azurerm_app_service.inspire.name
  resource_group_name = azurerm_resource_group.inspire.name
}

##                     ##
#  Create db on server  #
##                     ##
# resource "azurerm_sql_server" "example" {
#   name                         = var.sql_database_server_name
#   resource_group_name          = azurerm_resource_group.example.name
#   location                     = azurerm_resource_group.example.location
#   version                      = "12.0"
#   administrator_login          = "admin"
#   administrator_login_password = "Administrator123"
# }

# resource "azurerm_sql_firewall_rule" "main" {
#   name                = "AlllowAzureServices"
#   resource_group_name = azurerm_resource_group.main.name
#   server_name         = azurerm_sql_server.main.name
#   start_ip_address    = "0.0.0.0"
#   end_ip_address      = "0.0.0.0"
# }

# resource "azurerm_sql_elasticpool" "example" {
#   name                = "test"
#   resource_group_name = azurerm_resource_group.example.name
#   location            = azurerm_resource_group.example.location
#   server_name         = azurerm_sql_server.example.name
#   edition             = "Basic"
#   dtu                 = 10
#   db_dtu_min          = 0
#   db_dtu_max          = 10
#   pool_size           = 250  # in MBs, set it later 250*1024
# }
