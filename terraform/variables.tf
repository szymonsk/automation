variable "resource_group_name" {
  type        = string
  default     = "test_rg"
  description = "Name of the resource group."
}

variable "resource_group_location" {
  type        = string
  default     = "northcentralus"
  description = "Location of the resource group."
}

variable "storage_account_name" {
  type        = string
  default     = "teststorage3a6cb81d0"
  description = "Name of the resource group."
}

variable "blob_container_name" {
  type        = string
  default     = "teststoragecontainer3a6cb81d0"
  description = "Name of the resource group."
}

variable "app_service_plan_name" {
  type        = string
  default     = "test_service_plan"
  description = "Name of the resource group."
}

variable "app_service_name" {
  type        = string
  default     = "testapp"
  description = "Name of the app service."
}

variable "key_vault_name" {
  type        = string
  default     = "test_key_vault00001"
  description = "Name of the key vault."
}

variable "session_duration" {
  type        = number
  default     = 999
  description = "Name of the key vault."
}

variable "device_type_connection_string" {
  type        = string
  default     = "tablet"
  description = "Device type name."
}

variable "sql_server_name" {
  type        = string
  default     = "samplenamesqlserver"
  description = "Name of sql server."
}

variable "sql_database_name" {
  type        = string
  default     = "samplenamedb"
  description = "Name of the db."
}

variable "sql_database_server_name" {
  type        = string
  default     = "samplenamedb"
  description = "Name of the db server."
}

variable "custom_domain_app_service_url" {
  type        = string
  default     = "samplenamedb"
  description = "Name of the db."
}

variable "ip_restriction_address" {
  type        = string
  default     = "127.0.0.1"
  description = "restrictionaddress"
}
