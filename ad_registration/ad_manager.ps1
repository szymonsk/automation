############################
# Config
############################
# az login --tenant c6bb708b-e103-4bef-a560-ebf2d2690f16 # put here tenant id
# Connect-AzureAD -TenantId c6bb708b-e103-4bef-a560-ebf2d2690f16 # put here tenant id

$applicationName = "Originator-API"  # put here Originator App name
$applicationRG = ""  # put here resource group name inside which the apps are


Function GetApp([string] $Name)
{
    $theApp = (Get-AzureADApplication -Filter "DisplayName eq '$Name'")
    if ($theApp) {
        $appObjectId = $theApp.ObjectId
        $app = Get-AzureADMSApplication -ObjectId $appObjectId
        return $app
    } else {
        throw "No app named '$Name'"
    }
}

Function CreateAppRole([string] $Name)
{
    $appRole = New-Object Microsoft.Open.AzureAD.Model.AppRole
    # $appRole = New-Object Microsoft.Open.MSGraph.Model.AppRole
    $appRole.AllowedMemberTypes = New-Object System.Collections.Generic.List[string]
    $appRole.AllowedMemberTypes.Add("Application");
    $appRole.DisplayName = $Name
    $appRole.Id = New-Guid
    $appRole.IsEnabled = $true
    $appRole.Description = "Allow the application to read all things as itself."
    $appRole.Value = $Name;
    return $appRole
}


############################
# Get originator app
############################
$app = GetApp -Name $applicationName
$appID = $app.AppId
$appObjectIds = Get-AzureADApplication -Filter "AppId eq '$appID'"
$appObjectId = $appObjectIds.ObjectId

############################
# New application registration
############################
$appRegAD = New-AzureADApplication -DisplayName "$applicationName-registration" -AvailableToOtherTenants $false -ReplyUrls "https://localhost/"
$appRegADAppId = $appRegAD.AppId # App ID
$newApp = Get-AzureADMSApplication -ObjectId $appRegAD.ObjectId


############################
# Create app role
############################
$appRoles = $newApp.AppRoles
if ($appRoles.Count -eq 0) {
    Write-Host "Configuring new app role."
    $newAppRole = CreateAppRole -Name "Things.Read.All"
    $appRoles += $newAppRole
    Set-AzureADApplication -ObjectId $newApp.id -AppRoles $appRoles
    $appRoleId = $newAppRole.Id  # Role GUID
    $appRoles = $newApp.AppRoles
} else {
    Write-Host "App Roles already configured."
    return
}


############################
# Configure Application ID URI
############################
# $appIdUri = "https://swadmininspiresleep.onmicrosoft.com/$appRoleId"
$appIdUri = "api://test/$appRoleId"
Set-AzureADApplication -ObjectId $newApp.Id -IdentifierUris $appIdUri


############################
# Attach role assignment
############################
$apiOID = (Get-AzureADServicePrincipal -SearchString $newApp.DisplayName).ObjectId
# try { New-AzureADServiceAppRoleAssignment -ObjectId $appObjectId -Id $appRoleId -PrincipalId $appObjectId -ResourceId $apiOID
# } catch {
#     Write-Host "Admissible error ocurred, checking role assignment"
#     try { Get-AzureADServiceAppRoleAssignment -ObjectId $apiOID -All $true
#     } catch {
#         Write-Host "Role assignment failed."
#         return
#     }
# }

New-AzureADServiceAppRoleAssignment -ObjectId 81dd86ea-b48c-435f-ad84-c3aa93a66dac -Id $appRoleId -PrincipalId $appObjectId -ResourceId $apiOID


############################
# Add application settings
############################
# az webapp config appsettings set --name $applicationName --resource-group $applicationRG --settings <setting-name>="<value>